import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import * as React from 'react';
import {
  Platform,
  StatusBar,
  StyleSheet,
  View,
  ActivityIndicator,
} from 'react-native';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import reduxStore from './src/redux/store';
import i18n from './src/locales';

import useCachedResources from './src/hooks/useCachedResources';
import BottomTabNavigator from './navigation/BottomTabNavigator';
import LinkingConfiguration from './navigation/LinkingConfiguration';
import ProfileComponent from './src/modules/Profile/ProfileComponent';
import LoginComponent from './src/modules/Auth/LoginComponent';
import RegisterComponent from './src/modules/Auth/RegisterComponent';
import LoadingComponent from './src/modules/Auth/LoadingComponent';

const Stack = createStackNavigator();

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});

const Root = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        options={{ headerLeft: null }}
        name="Home"
        component={BottomTabNavigator}
      />
      <Stack.Screen
        options={{ headerBackTitle: i18n.t(`back`) }}
        name="Profile"
        component={ProfileComponent}
      />
    </Stack.Navigator>
  );
};

const App = () => {
  const isLoadingComplete = useCachedResources();
  if (!isLoadingComplete) {
    return null;
  }
  return (
    <View style={styles.container}>
      {Platform.OS === 'ios' && <StatusBar barStyle="dark-content" />}
      <NavigationContainer linking={LinkingConfiguration}>
        <Provider store={reduxStore.store}>
          <PersistGate
            loading={<ActivityIndicator />}
            persistor={reduxStore.persistor}
          >
            <Stack.Navigator>
              <Stack.Screen
                options={{ headerShown: false }}
                name="Loading"
                component={LoadingComponent}
              />
              <Stack.Screen
                options={{ headerShown: false }}
                name="Login"
                component={LoginComponent}
              />
              <Stack.Screen
                options={{ headerBackTitle: i18n.t(`back`) }}
                name="Register"
                component={RegisterComponent}
              />
              <Stack.Screen
                options={{ headerShown: false }}
                name="Root"
                component={Root}
              />
            </Stack.Navigator>
          </PersistGate>
        </Provider>
      </NavigationContainer>
    </View>
  );
};

export default App;

// export default from './src/storybook';
