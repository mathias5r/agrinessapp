import animalsService from '../../src/services/animals';

const mockedAnimals = [
  { nome: `Galinha`, localizacao: `Galinheiro` },
  { nome: `Porco`, localizacao: `Chiqueiro` },
  { nome: `Vaca`, localizacao: `Pasto` },
  { nome: `Bode`, localizacao: `Pasto` },
];

describe(`Test animals service`, () => {
  describe(`Test filterAnimalsByMatch method`, () => {
    test(`Should return animals by name`, () => {
      const result = animalsService.filterAnimalsByMatch({
        animals: mockedAnimals,
        match: 'g',
      });
      expect(result).toMatchObject([
        { nome: 'Galinha', localizacao: 'Galinheiro' },
      ]);
    });
    test(`Should return animals by location`, () => {
      const result = animalsService.filterAnimalsByMatch({
        animals: mockedAnimals,
        match: 'pasto',
      });
      expect(result).toMatchObject([
        {
          localizacao: 'Pasto',
          nome: 'Vaca',
        },
        {
          localizacao: 'Pasto',
          nome: 'Bode',
        },
      ]);
    });
  });
});
