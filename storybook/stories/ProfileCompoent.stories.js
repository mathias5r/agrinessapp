import React from 'react';
import { storiesOf } from '@storybook/react-native';
import { View } from 'react-native';
import { Provider as ReduxProvider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import ProfileComponent from '../../src/modules/Profile/ProfileComponent';

const mockStore = configureMockStore();
const store = mockStore({
  Animal: {
    updateResult: {},
  },
});

const mockedProps = {
  nome: 'SAX648',
  tipoAnimal: 'POULTRY',
  statusAnimal: '3',
  localizacao: 'Sala 5',
  dataNascimento: '2017-06-29 02:53',
  entradaPlantel: '2019-06-16',
  pesoCompra: '98.934',
  raca: 'ac-7077/m',
  codigoRastreamento: '742B7DC9863349D2A88A9AE6AC3DDABD',
  route: {
    params: {
      animal: {},
    },
  },
};

storiesOf(`ProfileComponent`, module)
  .addDecorator((getStory) => (
    <ReduxProvider store={store}>
      <View style={{ alignItems: `center`, flex: 1 }}>{getStory()}</View>
    </ReduxProvider>
  ))
  .add(`Initial`, () => <ProfileComponent {...mockedProps} />);
