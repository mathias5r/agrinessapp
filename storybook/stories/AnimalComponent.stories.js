import React from 'react';
import { storiesOf } from '@storybook/react-native';
import { View } from 'react-native';
import AnimalComponent from '../../src/modules/Animal/AnimalComponent';

const mockedProps = {
  nome: 'Cocoricó',
  tipoAnimal: 'Poultry',
  pesoCompra: `200,5`,
  localizacao: `Galpão 1`,
};

storiesOf(`AnimalComponent`, module)
  .addDecorator((getStory) => (
    <View style={{ alignItems: `center`, flex: 1 }}>{getStory()}</View>
  ))
  .add(`Initial`, () => <AnimalComponent {...mockedProps} />);
