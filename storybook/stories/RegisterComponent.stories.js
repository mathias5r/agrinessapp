import React from 'react';
import { storiesOf } from '@storybook/react-native';
import { View } from 'react-native';
import { Provider as ReduxProvider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import RegisterComponent from '../../src/modules/Auth/RegisterComponent';

const mockStore = configureMockStore();
const store = mockStore({
  Animal: {
    updateResult: {},
  },
  User: {
    credentials: {},
  },
});

storiesOf(`RegisterComponent`, module)
  .addDecorator((getStory) => (
    <ReduxProvider store={store}>
      <View style={{ alignItems: `center`, flex: 1 }}>{getStory()}</View>
    </ReduxProvider>
  ))
  .add(`Initial`, () => <RegisterComponent />);
