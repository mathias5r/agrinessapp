import React from 'react';
import { storiesOf } from '@storybook/react-native';
import { View } from 'react-native';
import { Provider as ReduxProvider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import LoginComponent from '../../src/modules/Auth/LoginComponent';

const mockStore = configureMockStore();
const store = mockStore({
  Animal: {
    updateResult: {},
  },
  User: {
    credentials: {},
  },
});

storiesOf(`LoginComponent`, module)
  .addDecorator((getStory) => (
    <ReduxProvider store={store}>
      <View style={{ alignItems: `center`, flex: 1 }}>{getStory()}</View>
    </ReduxProvider>
  ))
  .add(`Initial`, () => <LoginComponent />);
