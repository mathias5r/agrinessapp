const AnimalComponent = require(`./AnimalComponent.stories`);
const ProfileComponet = require(`./ProfileCompoent.stories`);
const LoginComponent = require('./LoginComponent.stories');
const RegisterComponent = require('./RegisterComponent.stories');

export default {
  AnimalComponent,
  ProfileComponet,
  LoginComponent,
  RegisterComponent,
};
