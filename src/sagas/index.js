import { takeLatest, all } from 'redux-saga/effects';
import {
  getAnimalsAsync,
  updateAnimalAsync,
  deleteAnimalAsync,
} from './animals';
import { execLoginAsync, execRegisterAsync } from './auth';

export default function* watchAll() {
  yield all([
    takeLatest('GET_ANIMALS', getAnimalsAsync),
    takeLatest('UPDATE_ANIMAL', updateAnimalAsync),
    takeLatest('DELETE_ANIMAL', deleteAnimalAsync),
    takeLatest('LOGIN', execLoginAsync),
    takeLatest('REGISTER', execRegisterAsync),
  ]);
}
