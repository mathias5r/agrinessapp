import { put } from 'redux-saga/effects';
import { setLoginResult } from '../redux/actions';
import authService from '../services/auth';
import logger from '../services/logger';

export function* execLoginAsync({ email, password }) {
  try {
    const token = yield authService.requestLogin({ email, password });
    yield put(setLoginResult(token));
  } catch (e) {
    logger.error(e);
    yield put(setLoginResult(null, 'login_error'));
  }
}

export function* execRegisterAsync({ name, email, password }) {
  try {
    const token = yield authService.requestRegister({ name, email, password });
    yield put(setLoginResult(token));
  } catch (e) {
    logger.error(e);
    yield put(setLoginResult(null, 'register_error'));
  }
}
