import { select, put } from 'redux-saga/effects';
import {
  setAnimals,
  setUpdateAnimalResult,
  setDeleteAnimalResult,
} from '../redux/actions';
import animalsService from '../services/animals';

const getAnimalsFromState = (state) => state.Animal.animals;
const getCredentials = (state) => state.User.credentials;

export function* getAnimalsAsync() {
  try {
    const credentials = yield select(getCredentials);
    const { value, skip } = yield select(getAnimalsFromState);
    const newAnimals = yield animalsService.requestAnimalsList({
      skip,
      Authorization: credentials?.value,
    });
    yield put(setAnimals([...value, ...newAnimals], null, skip + 10));
  } catch (e) {
    yield put(setAnimals([], e));
  }
}

export function* updateAnimalAsync({ id, nome, statusAnimal }) {
  try {
    const credentials = yield select(getCredentials);
    yield animalsService.requestUpdateAnimal({
      id,
      nome,
      statusAnimal,
      Authorization: credentials?.value,
    });
    const { value, skip, error } = yield select(getAnimalsFromState);
    const newAnimals = value.map((animal) => {
      if (animal.id === id) {
        return { ...animal, nome, statusAnimal };
      }
      return animal;
    });
    yield put(setAnimals(newAnimals, error, skip));
    yield put(setUpdateAnimalResult('Animal atualizado com sucesso!'));
  } catch (e) {
    yield put(setUpdateAnimalResult(null, e));
  }
}

export function* deleteAnimalAsync({ id }) {
  try {
    const credentials = yield select(getCredentials);
    yield animalsService.requestDeleteAnimal({
      id,
      Authorization: credentials?.value,
    });
    const { value, skip, error } = yield select(getAnimalsFromState);
    const newAnimals = value.filter((animal) => {
      return animal.id !== id;
    });
    yield put(setAnimals(newAnimals, error, skip));
    yield put(setDeleteAnimalResult('Animal deletado com sucesso!'));
  } catch (e) {
    yield put(setDeleteAnimalResult(``, e));
  }
}
