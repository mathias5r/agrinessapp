export const SET_ANIMALS = 'SET_ANIMALS';
export const SET_ANIMAL = 'SET_ANIMAL';
export const SET_CREATE_ANIMAL_RESULT = 'SET_CREATE_ANIMAL_RESULT';
export const SET_UPDATE_ANIMAL_RESULT = 'SET_UPDATE_ANIMAL_RESULT';
export const SET_DELETE_ANIMAL_RESULT = 'SET_DELETE_ANIMAL_RESULT';

export const GET_ANIMALS = 'GET_ANIMALS';
export const GET_ANIMAL = 'GET_ANIMAL';
export const CREATE_ANIMAL = 'CREATE_ANIMAL';
export const UPDATE_ANIMAL = 'UPDATE_ANIMAL';
export const DELETE_ANIMAL = 'DELETE_ANIMAL';

export const SET_LOGIN_RESULT = 'SET_LOGIN_RESULT';
export const SET_REGISTER_RESULT = 'SET_REGISTER_RESULT';

export const LOGIN = 'LOGIN';
export const REGISTER = 'REGISTER';

export const setAnimals = (value, error = null, skip) => ({
  type: SET_ANIMALS,
  animals: { value, error, skip },
});

export const setAnimal = (value, error = null) => ({
  type: SET_ANIMAL,
  animal: { value, error },
});

export const setCreateAnimalResult = (success = null, error = null) => ({
  type: SET_CREATE_ANIMAL_RESULT,
  createResult: { success, error },
});

export const setUpdateAnimalResult = (success = null, error = null) => ({
  type: SET_UPDATE_ANIMAL_RESULT,
  updateResult: { success, error },
});

export const setDeleteAnimalResult = (success = null, error = null) => ({
  type: SET_DELETE_ANIMAL_RESULT,
  deleteResult: { success, error },
});

export const setLoginResult = (value = null, error = null) => ({
  type: SET_LOGIN_RESULT,
  credentials: { value, error },
});

export const setRegisterResult = (value = null, error = null) => ({
  type: SET_REGISTER_RESULT,
  credentials: { value, error },
});

export const getAnimals = () => {
  return {
    type: GET_ANIMALS,
  };
};

export const getAnimal = () => ({
  type: GET_ANIMAL,
});

export const createAnimal = () => ({
  type: CREATE_ANIMAL,
});

export const updateAnimal = (id, nome, statusAnimal) => ({
  type: UPDATE_ANIMAL,
  id,
  nome,
  statusAnimal,
});

export const deleteAnimal = ({ id }) => ({
  type: DELETE_ANIMAL,
  id,
});

export const execLogin = ({ email, password }) => ({
  type: LOGIN,
  email,
  password,
});

export const execRegister = ({ name, email, password }) => ({
  type: REGISTER,
  name,
  email,
  password,
});
