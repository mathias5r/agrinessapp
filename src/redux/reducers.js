import {
  SET_ANIMALS,
  SET_ANIMAL,
  SET_CREATE_ANIMAL_RESULT,
  SET_UPDATE_ANIMAL_RESULT,
  SET_DELETE_ANIMAL_RESULT,
  SET_LOGIN_RESULT,
  SET_REGISTER_RESULT,
} from './actions';

const animalsInitialState = {
  animals: { value: [], error: null, skip: 0 },
  animal: { value: {}, error: null },
  createResult: { success: null, error: null },
  updateResult: { success: null, error: null },
  deleteResult: { success: null, error: null },
};

export const Animal = (state = animalsInitialState, action) => {
  switch (action.type) {
    case SET_ANIMALS:
      return {
        ...state,
        animals: action.animals,
      };
    case SET_ANIMAL:
      return {
        ...state,
        animal: action.animal,
      };
    case SET_CREATE_ANIMAL_RESULT:
      return {
        ...state,
        createResult: action.createResult,
      };
    case SET_UPDATE_ANIMAL_RESULT:
      return {
        ...state,
        updateResult: action.updateResult,
      };
    case SET_DELETE_ANIMAL_RESULT:
      return {
        ...state,
        deleteResult: action.deleteResult,
      };
    default:
      return state;
  }
};

const userInitialState = {
  credentials: { value: null, error: null },
};

export const User = (state = userInitialState, action) => {
  switch (action.type) {
    case SET_LOGIN_RESULT || SET_REGISTER_RESULT:
      return {
        ...state,
        credentials: action.credentials,
      };
    default:
      return state;
  }
};
