export const ENDPOINTS = {
  AGRINESS_SERVER: process.env.AGRINESS_SERVER || `http://localhost:8000`,
};

export const PROFILE_TITLES = {
  nome: 'Nome',
  statusAnimal: 'Status do Animal',
  tipoAnimal: 'Tipo do animal',
  pesoCompra: 'Peso de Compra',
  localizacao: 'Localização',
  codigoRastreamento: 'Código de Rastreio',
  raca: 'Raça',
  entradaPlantel: 'Entrada Plantel',
};

export const PROFILE_ICONS = {
  nome: 'exclamationcircleo',
  statusAnimal: 'retweet',
  tipoAnimal: 'profile',
  pesoCompra: 'totop',
  localizacao: 'home',
  codigoRastreamento: 'barcode',
  raca: 'profile',
  entradaPlantel: 'arrowright',
};

export const PROFILE_EDITABLE = {
  nome: true,
  statusAnimal: true,
  tipoAnimal: false,
  pesoCompra: false,
  localizacao: false,
  codigoRastreamento: false,
  raca: false,
  entradaPlantel: false,
};
