const tintColor = '#2f95dc';

export default {
  shadow: '#3668CB',
  main: `rgb(20,56,151)`,
  secondary: `rgb(54,104,203)`,
  dark_text: `white`,
  secondary_element: `gray`,
  tintColor,
  tabIconDefault: '#ccc',
  tabIconSelected: tintColor,
  tabBar: '#fefefe',
  errorBackground: 'red',
  errorText: '#fff',
  warningBackground: '#EAEB5E',
  warningText: '#666804',
  noticeBackground: tintColor,
  noticeText: '#fff',
};
