import { StyleSheet, Platform } from 'react-native';
import colors from '../constants/Colors';

const styles = StyleSheet.create({
  ios: {
    shadowColor: colors.shadow,
    shadowOffset: {
      width: 5,
      height: 5,
    },
    shadowOpacity: 0.22,
    shadowRadius: 11,
  },
  android: {
    elevation: 2,
  },
});

export default styles[Platform.OS];
