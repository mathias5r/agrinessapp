import styled from 'styled-components';

export const CenterView = styled.View`
  justify-content: center;
  align-items: center;
`;
