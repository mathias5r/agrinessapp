import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import colors from '../../constants/Colors';

const Text = styled.Text`
  font-size: ${({ textSize }) => `${textSize}px`};
  font-weight: bold;
  color: ${({ textColor }) => textColor};
  text-align: center;
`;

const Button = styled.TouchableOpacity`
  padding: 10px;
  margin: 10px;
  background-color: ${({ color }) => color};
  border-radius: 25px;
`;

const PillButton = ({ text, color, textColor, textSize, onPress }) => (
  <Button {...{ color, onPress }}>
    <Text {...{ textColor, textSize }}>{text}</Text>
  </Button>
);

PillButton.defaultProps = {
  text: 'Click',
  color: 'white',
  textColor: colors.main,
  textSize: 18,
};

PillButton.propTypes = {
  text: PropTypes.string,
  color: PropTypes.string,
  textColor: PropTypes.string,
  textSize: PropTypes.number,
  onPress: PropTypes.func.isRequired,
};

export default PillButton;
