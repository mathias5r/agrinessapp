import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import colors from '../../constants/Colors';

const Text = styled.Text`
  font-size: 18px;
  color: ${colors.dark_text};
`;

const Button = styled.TouchableOpacity`
  width: 100%;
  height: 10%;
  background-color: ${colors.secondary_element};
  align-items: center;
  justify-content: center;
  opacity: ${({ disabled }) => (disabled ? 0.5 : 1)};
`;

const BottomButton = ({ text, onPress, disabled }) => (
  <Button {...{ onPress, disabled }}>
    <Text>{text}</Text>
  </Button>
);

BottomButton.defaultProps = {
  disabled: false,
};

BottomButton.propTypes = {
  disabled: PropTypes.bool,
  text: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
};

export default BottomButton;
