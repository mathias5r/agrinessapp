import React from 'react';
import styled from 'styled-components';
import { AntDesign } from '@expo/vector-icons';
import PropTypes from 'prop-types';
import colors from '../constants/Colors';

const InputView = styled.View`
  width: 100%;
  flex-direction: row;
  justify-content: space-between;
  border-bottom-width: 1px;
  border-bottom-color: ${({ inputColor }) => inputColor};
`;

const IconView = styled.View`
  flex: 0.15;
  padding: 10px 0px;
  border-radius: 7px;
  justify-content: center;
  align-items: flex-start;
`;

const Input = styled.TextInput`
  flex: 0.85;
  padding: 10px 0px;
  border-radius: 7px;
  color: ${({ textColor }) => textColor};
`;

const Title = styled.Text`
  font-size: 12px;
  color: gray;
`;

export const MainInput = ({
  title,
  icon,
  iconColor,
  editable,
  value,
  onChangeText,
  placeholder,
  placeholderTextColor,
  inputColor,
  textColor,
  autoCapitalize,
  secureTextEntry,
}) => (
  <>
    {!!title && <Title>{title}</Title>}
    <InputView {...{ inputColor }}>
      {!!icon && (
        <IconView>
          <AntDesign name={icon} size={24} color={iconColor} />
        </IconView>
      )}
      <Input
        {...{
          secureTextEntry,
          autoCapitalize,
          textColor,
          placeholder,
          editable,
          value,
          onChangeText,
          placeholderTextColor,
        }}
      />
    </InputView>
  </>
);

MainInput.defaultProps = {
  title: ``,
  icon: ``,
  iconColor: ``,
  placeholder: ``,
  placeholderTextColor: `white`,
  editable: true,
  inputColor: colors.secondary_element,
  autoCapitalize: `sentences`,
  secureTextEntry: false,
  textColor: `white`,
};

MainInput.propTypes = {
  placeholder: PropTypes.string,
  placeholderTextColor: PropTypes.string,
  title: PropTypes.string,
  icon: PropTypes.string,
  iconColor: PropTypes.string,
  inputColor: PropTypes.string,
  editable: PropTypes.bool,
  onChangeText: PropTypes.func.isRequired,
  autoCapitalize: PropTypes.string,
  secureTextEntry: PropTypes.bool,
  textColor: PropTypes.string,
};
