import axios from 'axios';
import { ENDPOINTS } from '../constants/Config';

const requestLogin = async ({ email, password }) => {
  return axios
    .post(`${ENDPOINTS.AGRINESS_SERVER}/login`, { email, password })
    .then(({ data }) => data)
    .catch((e) => {
      throw new Error(`Error try to get animals: ${e}`);
    });
};

const requestRegister = async ({ name, email, password }) => {
  return axios
    .post(`${ENDPOINTS.AGRINESS_SERVER}/register`, { name, email, password })
    .then(({ data }) => data)
    .catch((e) => {
      throw new Error(`Error try to get animals: ${e}`);
    });
};

export default {
  requestLogin,
  requestRegister,
};
