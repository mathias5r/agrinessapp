import axios from 'axios';
import { ENDPOINTS } from '../constants/Config';
import logger from './logger';

const requestAnimalsList = async ({ skip, Authorization }) => {
  return axios
    .get(`${ENDPOINTS.AGRINESS_SERVER}/api/v1/animals?limit=10&skip=${skip}`, {
      headers: { Authorization },
    })
    .then(({ data }) => data)
    .catch((e) => {
      logger.error(e);
      throw new Error(`Error try to get animals: ${e}`);
    });
};

const requestUpdateAnimal = async ({
  id,
  nome,
  statusAnimal,
  Authorization,
}) => {
  return axios
    .patch(
      `${ENDPOINTS.AGRINESS_SERVER}/api/v1/animals/${id}`,
      {
        nome,
        statusAnimal,
      },
      { headers: { Authorization } }
    )
    .then(({ data }) => logger.info(data))
    .catch((e) => {
      logger.error(e);
      throw new Error(`Error try to get animals: ${e}`);
    });
};

const requestDeleteAnimal = async ({ id, Authorization }) => {
  return axios
    .delete(`${ENDPOINTS.AGRINESS_SERVER}/api/v1/animals/${id}`, {
      headers: { Authorization },
    })
    .then(({ data }) => logger.info(data))
    .catch((e) => {
      logger.error(e);
      throw new Error(`Error try to get animals: ${e}`);
    });
};

const filterAnimalsByMatch = ({ animals, match }) => {
  return animals.filter((item) => {
    return (
      item.nome.toLowerCase().includes(match.toLowerCase()) ||
      item.localizacao.toLowerCase().includes(match.toLowerCase())
    );
  });
};

const getInfoFromAnimal = ({ animal, name, status }) => {
  const { tipoAnimal, pesoCompra, localizacao, raca, entradaPlantel } = animal;
  return {
    nome: name,
    statusAnimal: status,
    tipoAnimal,
    pesoCompra,
    localizacao,
    raca,
    entradaPlantel,
  };
};

export default {
  requestAnimalsList,
  requestUpdateAnimal,
  requestDeleteAnimal,
  filterAnimalsByMatch,
  getInfoFromAnimal,
};
