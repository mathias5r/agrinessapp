import React, { useState } from 'react';
import styled from 'styled-components';
import { AntDesign } from '@expo/vector-icons';
import { useDispatch, useSelector } from 'react-redux';
import MessageModal from '../Modal/MessageModal';
import { updateAnimal, setUpdateAnimalResult } from '../../redux/actions';
import {
  PROFILE_ICONS,
  PROFILE_TITLES,
  PROFILE_EDITABLE,
} from '../../constants/Config';
import animalsService from '../../services/animals';
import BottomButtom from '../../library/Buttons/BottomButton';
import { MainInput } from '../../library/Inputs';
import colors from '../../constants/Colors';
import i18n from '../../locales';

const Container = styled.View`
  width: 100%;
  height: 100%;
  align-items: center;
  padding-top: 20px;
`;

const Scroll = styled.ScrollView`
  width: 100%;
  height: 100%;
  padding: 5% 5% 0px 5%;
`;

const InfoView = styled.View`
  padding: 2% 0;
`;

const Title = styled.Text`
  font-size: 12px;
  color: ${colors.secondary_element};
`;

const SETS = {
  nome: ({ text, setName, setSaveDisabled }) => {
    setSaveDisabled(false);
    setName(text);
  },
  statusAnimal: ({ text, setStatus, setSaveDisabled }) => {
    setSaveDisabled(false);
    setStatus(text);
  },
};

const ProfileComponent = ({ route }) => {
  const { animal } = route.params;
  const [name, setName] = useState(animal?.nome);
  const [status, setStatus] = useState(animal?.statusAnimal);
  const [saveDisabled, setSaveDisabled] = useState(true);
  const dispatch = useDispatch();
  const { success, error } = useSelector((state) => state.Animal.updateResult);
  const showModal = !!success || !!error;
  const info = animalsService.getInfoFromAnimal({ animal, name, status });
  return (
    <Container>
      <AntDesign name="barcode" size={24} color={colors.main} />
      <Title>{animal?.codigoRastreamento}</Title>
      <Scroll contentContainerStyle={{ paddingBottom: 30 }}>
        {Object.keys(info).map((key, index) => (
          <InfoView key={index.toString()}>
            <MainInput
              title={PROFILE_TITLES[key]}
              icon={PROFILE_ICONS[key]}
              iconColor={colors.main}
              editable={PROFILE_EDITABLE[key]}
              value={String(info[key])}
              textColor={
                PROFILE_EDITABLE[key] ? 'black' : colors.secondary_element
              }
              onChangeText={(text) =>
                SETS[key]({ setName, setStatus, setSaveDisabled, text })
              }
            />
          </InfoView>
        ))}
      </Scroll>
      <BottomButtom
        disabled={saveDisabled}
        onPress={() => dispatch(updateAnimal(animal.id, name, status))}
        text={i18n.t(`save`)}
      />
      {showModal && (
        <MessageModal
          message={success || error}
          visible
          setVisible={() => dispatch(setUpdateAnimalResult())}
        />
      )}
    </Container>
  );
};

export default ProfileComponent;
