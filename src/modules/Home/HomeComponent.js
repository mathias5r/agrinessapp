import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import AnimalComponent from '../Animal/AnimalComponent';
import {
  getAnimals,
  deleteAnimal,
  setDeleteAnimalResult,
} from '../../redux/actions';
import MessageModal from '../Modal/MessageModal';
import i18n from '../../locales';
import animalsService from '../../services/animals';

const Container = styled.View`
  flex: 1;
  align-items: center;
`;

const AnimalsList = styled.FlatList`
  flex: 1;
  width: 100%;
`;

const SearchBar = styled.TextInput`
  width: 100%;
  height: 10%;
  padding: 0 5%;
`;

const HomeComponent = ({ navigation }) => {
  const { value: animals, skip } = useSelector((state) => state.Animal.animals);
  const { success, error } = useSelector((state) => state.Animal.deleteResult);
  const [
    endReachedCalledDuringMomentum,
    setEndReachedCalledDuringMomentum,
  ] = useState(false);
  const [searchData, setSearchData] = useState([]);
  const [searchMode, setSearchMode] = useState(false);
  const dispacth = useDispatch();
  const showModal = success || error;
  return (
    <Container>
      <SearchBar
        placeholder={i18n.t('name_or_location')}
        onChangeText={(text) => {
          setSearchMode(true);
          setSearchData(
            animalsService.filterAnimalsByMatch({ animals, match: text })
          );
        }}
      />
      <AnimalsList
        data={searchMode ? searchData : animals}
        onEndReached={() => {
          if (!endReachedCalledDuringMomentum) {
            dispacth(getAnimals(animals, skip));
            setEndReachedCalledDuringMomentum(true);
          }
        }}
        onEndReachedThreshold={0}
        onMomentumScrollBegin={() => {
          setEndReachedCalledDuringMomentum(false);
        }}
        keyExtractor={(_, index) => index.toString()}
        renderItem={({ item }) => {
          return (
            <AnimalComponent
              onPress={() => navigation.navigate('Profile', { animal: item })}
              onLongPress={() => dispacth(deleteAnimal(item))}
              {...{ ...item }}
            />
          );
        }}
      />
      {showModal && (
        <MessageModal
          message={success || error}
          visible
          setVisible={() => dispacth(setDeleteAnimalResult())}
        />
      )}
    </Container>
  );
};

export default HomeComponent;
