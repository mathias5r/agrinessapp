import React from 'react';
import styled from 'styled-components';
import { LinearGradient } from 'expo-linear-gradient';
import { Entypo } from '@expo/vector-icons';
import { SCREEN_HEIGHT } from '../../constants/Layout';
import shadow from '../../styles/shadow';
import i18n from '../../locales';
import colors from '../../constants/Colors';

const Container = styled.TouchableOpacity`
  width: 100%;
  margin: 3% 0;
  align-items: center;
`;

const Card = styled.View`
  width: 90%;
  height: ${SCREEN_HEIGHT * 0.22}px;
  background-color: white;
  border-radius: 8px;
  padding: 15px;
`;

const Title = styled.Text`
  font-size: 20px;
  font-weight: bold;
`;

const Subtitle = styled.Text`
  font-size: 12px;
  font-weight: bold;
  color: gray;
`;

const InfoView = styled.View`
  flex: 0.6;
  flex-direction: row;
  padding: 3% 0;
`;

const TypeView = styled.View`
  flex: 1;
  justify-content: flex-start;
  align-items: flex-start;
`;

const WeightView = styled.View`
  flex: 1;
  justify-content: flex-start;
`;

const Divisor = styled.View`
  border-left-width: 1px;
  border-left-color: gray;
  padding: 0 5%;
`;

const SpecieView = styled(LinearGradient)`
  padding: 5px 10px;
  border-radius: 12px;
  align-items: flex-start;
  justify-content: center;
  margin: 5px 0;
`;

const SpecieText = styled.Text`
  font-size: 14px;
  font-weight: bold;
  color: white;
`;

const LocationView = styled.View`
  flex: 0.4;
  flex-direction: row;
  align-items: center;
`;

const LocationText = styled.Text`
  font-size: 12px;
  overflow: hidden;
`;

const AnimalComponent = ({
  nome,
  tipoAnimal,
  pesoCompra,
  localizacao,
  onPress,
  onLongPress,
}) => (
  <Container {...{ onLongPress, onPress }}>
    <Card style={shadow}>
      <Title>{nome}</Title>
      <InfoView>
        <TypeView>
          <Subtitle>{i18n.t(`animal-type`).toUpperCase()}</Subtitle>
          <SpecieView
            colors={[colors.main, colors.secondary]}
            start={[0, 1]}
            end={[1, 0]}
          >
            <SpecieText>{tipoAnimal}</SpecieText>
          </SpecieView>
        </TypeView>
        <WeightView>
          <Divisor>
            <Subtitle>{i18n.t(`weight`).toUpperCase()}</Subtitle>
            <Title>{`${pesoCompra} g`}</Title>
          </Divisor>
        </WeightView>
      </InfoView>
      <LocationView>
        <Entypo name="home" size={24} color={colors.main} />
        <LocationText>{localizacao}</LocationText>
      </LocationView>
    </Card>
  </Container>
);

export default AnimalComponent;
