import * as Yup from 'yup';

export const loginValidationSchema = Yup.object().shape({
  email: Yup.string().email(`invalid_email`).required(`empty_email`),
  password: Yup.string().required(`empty_password`).min(5, `min_password`),
});

export const registerValidationSchema = Yup.object().shape({
  username: Yup.string().required(`empty_name`),
  email: Yup.string().email(`invalid_email`).required(`empty_email`),
  password: Yup.string().required(`empty_password`).min(5, `min_password`),
});
