import React from 'react';
import styled from 'styled-components';
import { LinearGradient } from 'expo-linear-gradient';
import { useDispatch, useSelector } from 'react-redux';
import { useFormik } from 'formik';
import { execLogin } from '../../redux/actions';
import PillButton from '../../library/Buttons/PillButton';
import i18n from '../../locales';
import colors from '../../constants/Colors';
import { MainInput } from '../../library/Inputs';
import { loginValidationSchema } from './validations';

const Container = styled(LinearGradient)`
  width: 100%;
  height: 100%;
  justify-content: center;
  padding: 5% 5% 0 5%;
`;

const ButtonsView = styled.View`
  padding: 5% 0;
`;

const ErrorsView = styled.View`
  margin: 3px 0;
  height: 15px;
`;

const ErrorText = styled.Text`
  color: white;
  font-size: 12px;
  width: 100%;
`;

const AlertText = styled(ErrorText)`
  text-align: center;
`;

const LoginComponent = ({ navigation }) => {
  const { error } = useSelector((state) => state.User.credentials);
  const dispatch = useDispatch();
  const { errors, getFieldProps, handleSubmit, handleChange } = useFormik({
    initialValues: {
      email: ``,
      password: ``,
    },
    validationSchema: loginValidationSchema,
    validateOnChange: false,
    onSubmit: (values) => dispatch(execLogin(values)),
  });
  const { email, password } = getFieldProps(`email`, `password`);
  return (
    <Container
      colors={[colors.main, colors.secondary]}
      start={[0, 1]}
      end={[1, 0]}
    >
      <MainInput
        icon="user"
        name="user"
        iconColor="white"
        editable
        value={email}
        placeholder="Email"
        placeholderTextColor="white"
        autoCapitalize="none"
        inputColor="white"
        textColor="white"
        onChangeText={handleChange(`email`)}
      />
      <ErrorsView>
        {!!errors.email && <ErrorText>{i18n.t(errors.email)}</ErrorText>}
      </ErrorsView>
      <MainInput
        icon="key"
        name="password"
        iconColor="white"
        editable
        value={password}
        placeholder="Senha"
        placeholderTextColor="white"
        inputColor="white"
        textColor="white"
        secureTextEntry
        onChangeText={handleChange(`password`)}
      />
      <ErrorsView>
        {!!errors.password && <ErrorText>{i18n.t(errors.password)}</ErrorText>}
      </ErrorsView>
      <ButtonsView>
        <PillButton
          text="Login"
          color="white"
          textColor={colors.main}
          onPress={() => handleSubmit()}
        />
        <PillButton
          text={i18n.t(`register`)}
          color="white"
          textColor={colors.main}
          onPress={() => navigation.navigate(`Register`)}
        />
      </ButtonsView>
      <ErrorsView>
        {error === 'login_error' && <AlertText>{i18n.t(error)}</AlertText>}
      </ErrorsView>
    </Container>
  );
};

export default LoginComponent;
