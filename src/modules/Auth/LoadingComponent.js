import React from 'react';
import styled from 'styled-components';
import { LinearGradient } from 'expo-linear-gradient';
import { ActivityIndicator } from 'react-native';
import useAuth from '../../hooks/useAuth';
import colors from '../../constants/Colors';

const Container = styled(LinearGradient)`
  width: 100%;
  height: 100%;
  justify-content: center;
  align-items: center;
`;

const LoadingComponent = ({ navigation }) => {
  useAuth(navigation);
  return (
    <Container colors={[colors.main, colors.secondary]}>
      <ActivityIndicator color="white" size="large" />
    </Container>
  );
};

export default LoadingComponent;
