import React from 'react';
import styled from 'styled-components';
import { LinearGradient } from 'expo-linear-gradient';
import { useDispatch, useSelector } from 'react-redux';
import { useFormik } from 'formik';
import { execRegister } from '../../redux/actions';
import PillButton from '../../library/Buttons/PillButton';
import i18n from '../../locales';
import colors from '../../constants/Colors';
import { MainInput } from '../../library/Inputs';
import { registerValidationSchema } from './validations';

const Container = styled(LinearGradient)`
  width: 100%;
  height: 100%;
  justify-content: center;
  padding: 3% 5% 0 5%;
`;

const ButtonsView = styled.View`
  margin: 10px 0;
`;

const ErrorsView = styled.View`
  margin: 3px 0;
  height: 15px;
`;

const ErrorText = styled.Text`
  color: white;
  font-size: 12px;
  width: 100%;
`;

const AlertText = styled(ErrorText)`
  text-align: center;
`;

const RegisterComponent = () => {
  const { error } = useSelector((state) => state.User.credentials);
  const dispatch = useDispatch();
  const { errors, getFieldProps, handleSubmit, handleChange } = useFormik({
    initialValues: {
      username: ``,
      email: ``,
      password: ``,
    },
    validationSchema: registerValidationSchema,
    validateOnChange: false,
    onSubmit: ({ username, email, password }) => {
      dispatch(execRegister({ name: username, email, password }));
    },
  });
  const { username, email, password } = getFieldProps(
    `username`,
    `email`,
    `password`
  );
  return (
    <Container
      colors={[colors.main, colors.secondary]}
      start={[0, 1]}
      end={[1, 0]}
    >
      <MainInput
        icon="profile"
        iconColor="white"
        editable
        value={username}
        placeholder="Nome"
        placeholderTextColor="white"
        inputColor="white"
        textColor="white"
        onChangeText={handleChange(`username`)}
      />
      <ErrorsView>
        {!!errors.username && <ErrorText>{i18n.t(errors.username)}</ErrorText>}
      </ErrorsView>
      <MainInput
        icon="user"
        iconColor="white"
        editable
        value={email}
        placeholder="Email"
        placeholderTextColor="white"
        inputColor="white"
        textColor="white"
        autoCapitalize="none"
        onChangeText={handleChange(`email`)}
      />
      <ErrorsView>
        {!!errors.email && <ErrorText>{i18n.t(errors.email)}</ErrorText>}
      </ErrorsView>
      <MainInput
        icon="key"
        iconColor="white"
        editable
        value={password}
        placeholder="Senha"
        placeholderTextColor="white"
        inputColor="white"
        textColor="white"
        onChangeText={handleChange(`password`)}
      />
      <ErrorsView>
        {!!errors.password && <ErrorText>{i18n.t(errors.password)}</ErrorText>}
      </ErrorsView>
      <ButtonsView>
        <PillButton
          text={i18n.t(`save`)}
          color="white"
          textColor={colors.main}
          onPress={handleSubmit}
        />
      </ButtonsView>
      <ErrorsView>
        {error === 'register_error' && <AlertText>{i18n.t(error)}</AlertText>}
      </ErrorsView>
    </Container>
  );
};

export default RegisterComponent;
