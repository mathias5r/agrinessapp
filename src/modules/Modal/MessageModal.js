import React from 'react';
import { Modal } from 'react-native';
import styled from 'styled-components';

const Center = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
`;

const Body = styled.View`
  height: 20%;
  width: 80%;
  background-color: white;
  border-radius: 25px;
  justify-content: center;
  align-items: center;
`;

const CloseButton = styled.TouchableOpacity``;

const Text = styled.Text`
  font-size: 16px;
  padding: 20px;
`;

const MessageModal = ({ message, visible, setVisible }) => {
  return (
    <Modal
      animationType="fade"
      transparent
      visible={visible}
      onRequestClose={() => setVisible(false)}
    >
      <Center>
        <Body>
          <Text>{message}</Text>
          <CloseButton onPress={() => setVisible(false)}>
            <Text>OK</Text>
          </CloseButton>
        </Body>
      </Center>
    </Modal>
  );
};

export default MessageModal;
