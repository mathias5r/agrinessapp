import * as React from 'react';
import styled from 'styled-components';
import { useDispatch } from 'react-redux';
import { StackActions } from '@react-navigation/native';
import { setLoginResult } from '../../redux/actions';
import { CenterView } from '../../library/Views';
import PillButton from '../../library/Buttons/PillButton';

const Container = styled(CenterView)`
  width: 100%;
  height: 100%;
`;

const SettingsComponent = ({ navigation }) => {
  const dispatch = useDispatch();
  return (
    <Container>
      <PillButton
        text="Logout"
        onPress={() => {
          dispatch(setLoginResult());
          navigation.dispatch(StackActions.popToTop());
        }}
      />
    </Container>
  );
};

export default SettingsComponent;
