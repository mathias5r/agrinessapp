import { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { getAnimals } from '../redux/actions';

export default (navigation) => {
  const credentials = useSelector((state) => state.User.credentials);
  const animals = useSelector((state) => state.Animal.animals);
  const dispatch = useDispatch();
  if (animals?.value?.length) {
    navigation.navigate('Root');
  }
  useEffect(() => {
    if (credentials?.value) {
      dispatch(getAnimals());
    } else {
      navigation.navigate('Login');
    }
  }, [credentials]);
};
