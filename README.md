# Agriness App

App to manage animals.

Backend server: [AgrinessServer](https://bitbucket.org/mathias5r/agrinessserver/src/master/)


## Requirements

### Expo

* npm install -g expo-cli


## Configuring

In ./src/constants/Config.js edit the agriness server endpoint:

```bash
AGRINESS_SERVER: process.env.AGRINESS_SERVER || `http://localhost:8000`
```

## Testing

```bash
yarn test
```
